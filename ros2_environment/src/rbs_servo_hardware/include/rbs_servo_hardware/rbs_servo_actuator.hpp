#ifndef rbs_servo_hardware__RBS_ACTUATOR_HPP_
#define rbs_servo_hardware__RBS_ACTUATOR_HPP_
// Libs for fake connection 
// WARN: Delete it in the future
#include <netinet/in.h>
#include <rclcpp/logger.hpp>
#include <sys/socket.h>
// System libs
#include <memory>
#include <string>
#include <vector>
// ROS specific libs
#include "hardware_interface/actuator_interface.hpp"
#include "hardware_interface/handle.hpp"
#include "hardware_interface/hardware_info.hpp"
#include "hardware_interface/system_interface.hpp"
#include "hardware_interface/types/hardware_interface_return_values.hpp"
#include "rclcpp/macros.hpp"
#include "rbs_servo_hardware/visibility_control.h"

namespace rbs_servo_hardware
{
class RbsServoActuator : public hardware_interface::ActuatorInterface
{
public:
  RCLCPP_SHARED_PTR_DEFINITIONS(RbsServoActuator);

  rbs_servo_hardware_PUBLIC
  hardware_interface::CallbackReturn on_init(
    const hardware_interface::HardwareInfo & info) override;

  rbs_servo_hardware_PUBLIC
  std::vector<hardware_interface::StateInterface> export_state_interfaces() override;

  rbs_servo_hardware_PUBLIC
  std::vector<hardware_interface::CommandInterface> export_command_interfaces() override;

  rbs_servo_hardware_PUBLIC
  hardware_interface::CallbackReturn on_activate(
    const rclcpp_lifecycle::State & previous_state) override;

  rbs_servo_hardware_PUBLIC
  hardware_interface::CallbackReturn on_deactivate(
    const rclcpp_lifecycle::State & previous_state) override;

  rbs_servo_hardware_PUBLIC
  hardware_interface::CallbackReturn on_shutdown(
    const rclcpp_lifecycle::State & previous_state) override;

  rbs_servo_hardware_PUBLIC
  hardware_interface::return_type read(
    const rclcpp::Time & time, const rclcpp::Duration & period) override;

  rbs_servo_hardware_PUBLIC
  hardware_interface::return_type write(
    const rclcpp::Time & time, const rclcpp::Duration & period) override;

private:
  // Parameters for the rbs_servo_hardware simulation
  // double hw_start_sec_;
  // double hw_stop_sec_;
  
  rclcpp::Logger logger_ = rclcpp::get_logger("rbs_servo_hardware");

  // Store the command for the robot
  double hw_joint_target_angle_;
  double hw_joint_target_velocity_;
  // Store the states for the robot
  double hw_joint_angle_;
  double hw_joint_velocity_;

  // Fake "mechanical connection" between actuator and sensor using sockets
  // struct sockaddr_in address_;
  // int socket_port_;
  // int sockoptval_ = 1;
  // int sock_;

  std::string can_interface_;
  int can_node_id_;
  int can_socket_;
};

}  // namespace rbs_servo_hardware

#endif  // rbs_servo_hardware__RBS_ACTUATOR_HPP_
