# CAN Communication Scripts

This repository contains Python scripts for testing and interacting with a CAN bus system. These scripts enable sending and receiving CAN messages to control a motor, set angles, and adjust velocities.

## Prerequisites

1. **Python 3.7+** installed on your system.
2. **`python-can` library** installed. Install it via pip:
   ```bash
   pip install python-can
   ```
3. **SocketCAN interface** properly configured on your Linux system. The default channel is `can0`.

## Usage

### 1. Receiving CAN Messages

The script `python_can.py` listens to the CAN bus and processes incoming messages.

#### Run:
```bash
python3 python_can.py
```

#### Features:
- Processes messages with data length 5.
- Parses the first byte (`flag`) to determine the type:
  - `'A'`: Angle (float).
  - `'V'`: Velocity (float).
  - `'E'`: Enable/disable status (boolean).

### 2. Enabling or Disabling the Motor

The script `python_enable_motor.py` sends commands to enable or disable the motor.

#### Run:
```bash
python3 python_enable_motor.py <0|1>
```

#### Arguments:
- `0`: Disable the motor.
- `1`: Enable the motor.

### 3. Sending Target Angle

The script `python_send_angle.py` sends a target angle to the CAN bus.

#### Run:
```bash
python3 python_send_angle.py
```

#### Behavior:
- Sends a message with a predefined target angle every second.
- Adjust the target angle in the script (`target_angle` variable).

### 4. Sending Target Velocity

The script `python_send_velocity.py` sends a target velocity to the CAN bus.

#### Run:
```bash
python3 python_send_velocity.py
```

#### Behavior:
- Sends a message with a predefined target velocity every second.
- Adjust the target velocity in the script (`target_speed` variable).

## Configuration

### CAN Interface
The scripts use the following default CAN bus settings:
- **Channel**: `can0`
- **Bitrate**: `1 Mbps`

If your configuration differs, update the `Bus()` initialization in the scripts.

## Troubleshooting

1. **"Error initializing CAN bus"**:
   - Ensure your CAN interface is correctly configured and active:
     ```bash
     sudo ip link set can0 up type can bitrate 1000000
     ```
