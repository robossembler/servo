import can
import struct
import time
import argparse

# Function to send the target angle
def send_target_angle(bus, target_angle):
    msg = can.Message()
    msg.arbitration_id = 1  # Message ID
    msg.is_extended_id = False
    msg.dlc = 5  # Message length
    msg.data = [ord('A')] + list(struct.pack('<f', target_angle))  # 'A' for the command identifier, followed by the angle in float format

    try:
        bus.send(msg)
        print(f"Sent message with target angle: {target_angle} degrees")
        print(f"Message data: {msg.data}")
    except can.CanError:
        print("Message failed to send")

# Main function
def main():
    parser = argparse.ArgumentParser(description="Send target angles over CAN bus.")
    parser.add_argument("--angle", type=float, required=True, help="Target angle to send over the CAN bus")
    args = parser.parse_args()

    target_angle = args.angle

    # CAN interface setup
    bus = can.interface.Bus(channel='can0', bustype='socketcan', bitrate=1000000)  # Ensure the bitrate matches the microcontroller settings
    print("CAN bus initialized, sending target angles...")

    # Loop to send messages
    send_target_angle(bus, target_angle)

if __name__ == '__main__':
    main()
