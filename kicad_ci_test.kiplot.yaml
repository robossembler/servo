# Example Kibot config file
kibot:
  version: 1

preflight:
  run_erc: true
  update_xml: true
  run_drc: true
  check_zone_fills: false
  ignore_unconnected: true

global:
  # We want the revision added to the names for this project
  output: '%f-%i_%r.%x'
  hide_excluded: true

#   kiauto_wait_start: 60
#   kiauto_time_out_scale: 2

outputs:
  - name: 'print_sch'
    comment: "Print schematic (PDF)"
    type: pdf_sch_print
    dir: .
    options:
      output: Schematic.pdf

  - name: 'print_front'
    comment: "Print F.Cu+Dwgs.User"
    type: pdf_pcb_print
    dir: .
    options:
      output_name: PCB_Top.pdf
    layers:
      - layer: B.Cu
      - layer: F.SilkS

  - name: 'print_bottom'
    comment: "Print B.Cu+Dwgs.User"
    type: pdf_pcb_print
    dir: .
    layers:
      - layer: B.Cu
      - layer: Dwgs.User

  - name: 'gerbers'
    comment: "Gerbers for the board house"
    type: gerber
    dir: Gerbers
    options:
      # generic layer options
      exclude_edge_layer: true
      exclude_pads_from_silkscreen: false
      use_aux_axis_as_origin: false
      plot_sheet_reference: false
      plot_footprint_refs: true
      plot_footprint_values: true
      force_plot_invisible_refs_vals: false
      tent_vias: true

      # gerber options
      line_width: 0.1
      subtract_mask_from_silk: false
      use_protel_extensions: false
      gerber_precision: 4.6
      create_gerber_job_file: true
      use_gerber_x2_attributes: true
      use_gerber_net_attributes: true

    layers:
      - 'selected'
      # - layer: B.Cu
      #   suffix: B_Cu
      # - layer: F.SilkS
      #   suffix: F_SilkS
      # - layer: Edge.Cuts
      #   suffix: Edge_Cuts

  - name: 'interactive_bom'
    comment: "Interactive Bill of Materials (HTML)"
    type: ibom
    dir: BoM
    options:
      hide_excluded: true
      blacklist: 'DNF*'
      name_format: '%f_%r_iBoM'

  - name: 'bom_html'
    comment: "Bill of Materials in HTML format"
    type: bom
    dir: BoM
    options:
      format: HTML  # HTML or CSV

  - name: 'bom_csv'
    comment: "Bill of Materials in CSV format"
    type: bom
    dir: BoM
    options:
      format: CSV  # HTML or CSV
      ref_separator: ','
      normalize_values: true
      normalize_locale: true
      csv: # Hide Project Info & Statistics for PCB manufacturer file processing
        hide_pcb_info: true
        hide_stats_info: true
      columns:
        - Row
        - field: manf#
          name: Manufacturer Part Number
        - field: 'Quantity Per PCB'
          name: Qty
        - field: References
          name: Designator
        - Part
        - Value
        - Footprint
        - Description
        - Datasheet

  - name: 'bom_xlsx'
    comment: "Bill of Materials in XLSX format"
    type: bom
    dir: BoM
    options:
      format: XLSX

  - name: excellon_drill
    comment: "Excellon drill files"
    type: excellon
    dir: Drill
    options:
      metric_units: false
      pth_and_npth_single_file: false
      use_aux_axis_as_origin: false
      minimal_header: false
      mirror_y_axis: false
      report:
        filename: 'spora-drl.rpt'
      map:
        type: 'pdf'

  - name: gerber_drills
    comment: "Gerber drill files"
    type: gerb_drill
    dir: Drill
    options:
      use_aux_axis_as_origin: false

  - name: 'position'
    comment: "Pick and place file"
    type: position
    dir: Position
    options:
      format: ASCII   # CSV or ASCII format
      units: millimeters  # millimeters or inches
      separate_files_for_front_and_back: true
      only_smd: true

  - name: pcb_top_g
    comment: "PCB render top green"
    type: pcbdraw
    dir: PCB/green
    options:
      format: jpg
      show_components: none
      dpi: 600

  - name: pcb_bot_g
    comment: "PCB render bottom green"
    type: pcbdraw
    dir: PCB/green
    options:
      format: jpg
      bottom: True
      show_components: none
      dpi: 600

  - name: pcb_top_b
    comment: "PCB render top blue"
    type: pcbdraw
    dir: PCB/blue
    options:
      format: jpg
      style: set-blue-enig
      show_components: none
      dpi: 600

  - name: pcb_bot_b
    comment: "PCB render bottom blue"
    type: pcbdraw
    dir: PCB/blue
    options:
      format: jpg
      style: set-blue-enig
      bottom: True
      show_components: none
      dpi: 600

  - name: pcb_top_r
    comment: "PCB render top red"
    type: pcbdraw
    dir: PCB/red
    options:
      format: jpg
      style: set-red-enig
      show_components: none
      dpi: 600

  - name: pcb_bot_r
    comment: "PCB render bottom red"
    type: pcbdraw
    dir: PCB/red
    options:
      format: jpg
      style: set-red-enig
      bottom: True
      show_components: none
      dpi: 600

  - name: step
    comment: "Generate 3D model (STEP)"
    type: step
    dir: 3D
    options:
      metric_units: true
      origin: 85,66.2

# Gerber and drill files for PCBWay, with stencil (solder paste)
# URL: https://www.pcbway.com
# Based on setting used by Gerber Zipper (https://github.com/g200kg/kicad-gerberzipper)
  - name: PCBWay_gerbers
    comment: Gerbers compatible with PCBWay
    type: gerber
    dir: PCBWay
    options: &gerber_options
      exclude_edge_layer: true
      exclude_pads_from_silkscreen: true
      plot_sheet_reference: false
      plot_footprint_refs: true
      plot_footprint_values: true
      force_plot_invisible_refs_vals: false
      tent_vias: true
      use_protel_extensions: true
      create_gerber_job_file: false
      output: "%f.%x"
      gerber_precision: 4.6
      use_gerber_x2_attributes: false
      use_gerber_net_attributes: false
      disable_aperture_macros: true
      line_width: 0.1
      subtract_mask_from_silk: false
      inner_extension_pattern: '.gl%N'
    layers:
      - 'selected'

  - name: PCBWay_drill
    comment: Drill files compatible with PCBWay
    type: excellon
    dir: PCBWay
    options:
      metric_units: false
      minimal_header: true
      zeros_format: SUPPRESS_LEADING
      # left_digits: 3
      # right_digits: 3
      # See https://github.com/INTI-CMNB/kicad-ci-test-spora/issues/1
      # and https://docs.oshpark.com/design-tools/gerbv/fix-drill-format/
      left_digits: 2
      right_digits: 4
      pth_and_npth_single_file: false
      pth_id: ''
      npth_id: '-NPTH'
      output: "%f%i.drl"

  - name: PCBWay
    comment: ZIP file for PCBWay
    type: compress
    dir: PCBWay
    options:
      format: ZIP
      files:
        - from_output: PCBWay_gerbers
          dest: /
        - from_output: PCBWay_drill
          dest: /
